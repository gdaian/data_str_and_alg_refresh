import time

global_var = []

def print_numbers_version_one():
    number = 2

    while number <= 1000:
        if number % 2 == 0:
            print(number)
        number += 1

def print_numbers_version_two():
    number = 2

    while number <= 1000:
        print(number)
        number += 2

def print_numbers_personal_version_one():
    number = 1000
    
    for nr in list(map(lambda number: number*2, range(number//2))):
        print(nr)
    
def print_numbers_personal_version_two():
    for number in range(2, 1000, 2):
        print(number)

def print_numbers_personal_version_three():
    even_numbers = [number for number in range(2,1000,2)]
    for number in even_numbers:
        print(number)

def print_numbers_personal_version_four():
    even_numbers = list(filter(lambda x : x % 2 == 0, range(2, 1000, 2)))
    for number in even_numbers:
        print(number)

def calculate_function_duration(func):
    start = time.time()
    func()
    end = time.time()
    global_var.append(func.__name__ + " took " + str(1000 * (end-start)) + " milliseconds")

if __name__=="__main__":
    calculate_function_duration(print_numbers_version_one)
    calculate_function_duration(print_numbers_version_two)
    calculate_function_duration(print_numbers_personal_version_one)
    calculate_function_duration(print_numbers_personal_version_two)
    calculate_function_duration(print_numbers_personal_version_three)
    for duration in global_var:
        print(duration)