# Insert -> at most n steps
pythonArray = ["apples", "bananas", "cucumbers", "dates", "elderberries"]
# Insert -> at most 2n+1 steps. Delete, search, get -> Same as array
pythonSet = {"apples", "bananas", "cucumbers", "dates", "elderberries"}
